from starlette.testclient import TestClient

from app.main import app

client = TestClient(app)


def test_sign_up():
    user = {
        "username": "username",
        "password": "password",
        "first_name": None,
        "last_name": None
    }
    response = client.post("/account/sign_up", json=user)
    assert response.status_code == 200
