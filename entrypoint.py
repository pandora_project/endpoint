import os
import sys

import uvicorn

from project.manage import app

if __name__ == "__main__":
    os.system('service atd start')
    host = os.environ["REST_HOST"]
    port = int((os.environ["REST_PORT"]))
    uvicorn.run(app, host=host, port=port, debug=True)
