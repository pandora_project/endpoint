export $(cat ./env/local.env)
docker-compose -f docker-compose.local.yml down
docker-compose -f docker-compose.local.yml build
docker-compose -f docker-compose.local.yml up

# to run only database
# docker-compose -f docker-compose.local.yml run --service-ports database