USERNAME = $(whoami)

sudo chown -R $USERNAME:$USERNAME volumes/
sudo chmod -R 777 volumes/

pipenv run python -c '\
from init_db.loader.loader import load_all
from project.utils.library_volume import initiate_volume; \
\
initiate_volume(); \
load_all()'
