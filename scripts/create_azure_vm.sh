sub=$(az account show --query "id" -o tsv)
docker-machine create -d azure \
    --azure-subscription-id $sub \
    --azure-ssh-user azureuser \
    --azure-open-port 80 \
    --azure-size "Standard_B1s" \
    --azure-location "West Europe" \
    --azure-resource-group "docker-machine" \
    --azure-image "canonical:UbuntuServer:18.04.0-LTS:latest" \
    myvm