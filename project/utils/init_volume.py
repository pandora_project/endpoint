import os

from project.database import connection, models
from project.settings.base import VOLUMES_PATH


def init_volume():
    models.Base.metadata.create_all(bind=connection.engine)
