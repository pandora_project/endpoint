import os
from pathlib import Path

import ansible_runner


class Switch():
    playbooks_path = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(Path(__file__).absolute()))), 'playbooks')

    def is_running(self):
        play_path = os.path.join(Switch.playbooks_path, 'is_running.yml')
        r = ansible_runner.run(playbook=play_path)
        return len(r.stats['failures']) == 0
    
    def create_vm(self):
        play_path = os.path.join(Switch.playbooks_path, 'create_vm.yml')
        r = ansible_runner.run(playbook=play_path)
        return len(r.stats['failures']) == 0

    def start_service(self):
        play_path = os.path.join(Switch.playbooks_path, 'start_service.yml')
        r = ansible_runner.run(playbook=play_path)
        return len(r.stats['failures']) == 0

    def start_vm(self):
        play_path = os.path.join(Switch.playbooks_path, 'start_vm.yml')
        r = ansible_runner.run(playbook=play_path)
        return len(r.stats['failures']) == 0

    def stop_vm(self):
        play_path = os.path.join(Switch.playbooks_path, 'stop_vm.yml')
        r = ansible_runner.run(playbook=play_path)
        return len(r.stats['failures']) == 0
