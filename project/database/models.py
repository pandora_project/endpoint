from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Table, DateTime
from sqlalchemy.orm import relationship

from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class User(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    password = Column(String)
    username = Column(String, unique=True)
    disabled = Column(Boolean, default=True)
    first_name = Column(String)
    last_name = Column(String)

    tasks = relationship('Task')


class Task(Base):
    __tablename__ = "task"

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    image_url = Column(String)
    response = Column(String)
    date = Column(DateTime)
