from datetime import timedelta
import json

from fastapi import APIRouter, Depends, Body, HTTPException, Form
from fastapi.security import OAuth2PasswordRequestForm
from starlette.status import HTTP_401_UNAUTHORIZED, HTTP_303_SEE_OTHER, HTTP_409_CONFLICT, HTTP_205_RESET_CONTENT, HTTP_200_OK, HTTP_201_CREATED
from starlette.requests import Request
from starlette.responses import RedirectResponse, Response, JSONResponse
from sqlalchemy.orm import Session
from pydantic import BaseModel

from project.database import models
from project.database.connection import SessionFactory
from project.api.security import ACCESS_TOKEN_EXPIRE_MINUTES, create_access_token, authenticate_user, get_password_hash
from project.manage import templates

router = APIRouter()


@router.get("/", name="sign_up")
async def render_template(request: Request):
    return templates.TemplateResponse("sign_up.html",
                                      context={"request": request})


@router.post("/", status_code=201)
async def create_new_account(username: str = Form(...),
                             password: str = Form(...),
                             first_name: str = Form(''),
                             last_name: str = Form('')):
    password = get_password_hash(password)
    with SessionFactory() as session:
        if session.query(models.User).filter_by(username=username).first():
            raise HTTPException(status_code=HTTP_200_OK,
                                detail="Username is already occupied")
        db_user = models.User(password=password,
                              username=username,
                              first_name=first_name,
                              last_name=last_name)
        session.add(db_user)
        session.commit()
        session.refresh(db_user)
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(data={"sub": username},
                                       expires_delta=access_token_expires)
    access_token = str(access_token, 'utf-8')
    cookie_age = "604800"
    content = {
        "cookie": [
            "Authorization=Bearer " + access_token + "; Path=/; max-age=" +
            cookie_age + ";", "UserId=" + str(db_user.id) +
            "; Path=/; max-age=" + cookie_age + ";", "UserName=" +
            str(db_user.username) + "; Path=/; max-age=" + cookie_age + ";"
        ]
    }
    return JSONResponse(content=content, status_code=HTTP_201_CREATED)
