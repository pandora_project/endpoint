from fastapi import APIRouter
from . import sign_in, sign_up

router = APIRouter()
router.include_router(sign_up.router, prefix='/sign_up')
router.include_router(sign_in.router, prefix='/sign_in')
