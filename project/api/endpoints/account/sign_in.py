from datetime import timedelta

from fastapi import APIRouter, Depends, Body, HTTPException, Form
from fastapi.security import OAuth2PasswordRequestForm
from starlette.status import HTTP_401_UNAUTHORIZED, HTTP_201_CREATED
from starlette.requests import Request
from starlette.responses import JSONResponse
from sqlalchemy.orm import Session
from pydantic import BaseModel

from project.database import models
from project.database.connection import SessionFactory
from project.api.security import ACCESS_TOKEN_EXPIRE_MINUTES, create_access_token, authenticate_user, get_password_hash
from project.manage import templates

router = APIRouter()


@router.get("/", name="sign_in")
async def render_template(request: Request):
    return templates.TemplateResponse("sign_in.html",
                                      context={"request": request})


@router.post("/")
async def get_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = await authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(data={"sub": user.username},
                                       expires_delta=access_token_expires)
    access_token = str(access_token, 'utf-8')
    cookie_age = "604800"
    content = {
        "cookie": [
            "Authorization=Bearer " + access_token + "; Path=/; max-age=" +
            cookie_age + ";",
            "UserId=" + str(user.id) + "; Path=/; max-age=" + cookie_age + ";",
            "UserName=" + str(user.username) + "; Path=/; max-age=" +
            cookie_age + ";"
        ]
    }
    return JSONResponse(content=content, status_code=HTTP_201_CREATED)