from typing import List, Dict
import datetime
import json
import os
from urllib.request import urlretrieve
import urllib
import time

from fastapi import APIRouter, Depends, Body, Form
from project.manage import templates
from starlette.requests import Request
from starlette.responses import JSONResponse, RedirectResponse
from starlette.status import HTTP_201_CREATED, HTTP_200_OK
from starlette.websockets import WebSocket
from sqlalchemy import func, desc
import pika
from crontab import CronTab

from project.api.security import get_current_active_user, models
from project.database.connection import SessionFactory
from project.database import models
from project.vm_switch import Switch

router = APIRouter()


@router.get("/", name="home")
async def authors(request: Request):
    """
    Render chanels template. If user in not authenticated redirect to sign_in page.
    """
    user_id_cookie = 'UserId'
    not_authenticated_response = RedirectResponse(request.url_for('sign_in'))

    if 'cookie' not in request.headers:
        return not_authenticated_response
    cookies = request.headers['cookie']
    cookies = {
        key.strip(): value
        for key, value in [cooky.split('=') for cooky in cookies.split(';')]
    }
    if user_id_cookie not in cookies:
        return not_authenticated_response

    return templates.TemplateResponse("home.html", context={'request': request})


@router.post("/", status_code=HTTP_201_CREATED)
async def create_task(user: models.User = Depends(get_current_active_user),
                      image_url: str = Form(...)):
    """
    Add task to database, message broker. Run slave VM if it's need.
    """

    def add_task(image_url: str):
        """
        Add task to message broker
        """
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=os.environ['MQ_IP'],
                                      port=os.environ['MQ_PORT']))
        channel = connection.channel()

        channel.queue_declare(queue='task_queue')

        channel.basic_publish(exchange='',
                              routing_key='task_queue',
                              body=image_url)
        connection.close()

    def vm_is_reachable():
        if not os.system('ping -c 1 -W 1 ' + os.environ['SLAVE_IP']):
            return True
        return False

    try:
        urlretrieve(image_url)
    except urllib.error.HTTPError as e:
        return JSONResponse(content=str(e), status_code=200)
    #else:
    #    return JSONResponse(content='Unrecognized error', status_code=200)

    with SessionFactory() as session:
        task = models.Task(user_id=user.id,
                           response='not done yet',
                           image_url=image_url,
                           date=datetime.datetime.now())
        session.add(task)
        session.commit()
        session.refresh(task)
    task = json.dumps({'id': task.id, 'url': image_url})
    add_task(task)

    if not vm_is_reachable():
        switch = Switch()
        switch.start_vm()
        os.system('at -f stop_vm.sh now +50 minutes')
        while not vm_is_reachable():
            time.sleep(1)
        switch.start_service()
