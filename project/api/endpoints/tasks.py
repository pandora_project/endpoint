from typing import List, Dict
import datetime
import json

from fastapi import APIRouter, Depends, Body
from project.manage import templates
from pydantic import BaseModel
from starlette.requests import Request
from starlette.responses import JSONResponse, RedirectResponse
from starlette.status import HTTP_201_CREATED, HTTP_200_OK
from starlette.websockets import WebSocket
from sqlalchemy import func, desc

from project.api.security import get_current_active_user, models
from project.database.connection import SessionFactory
from project.database import models

router = APIRouter()


@router.get("/", name="tasks")
async def authors(request: Request):
    """
    Render chanels template. If user in not authenticated redirect to sign_in page.
    """
    user_id_cookie = 'UserId'
    not_authenticated_response = RedirectResponse(request.url_for('sign_in'))

    if 'cookie' not in request.headers:
        return not_authenticated_response
    cookies = request.headers['cookie']
    cookies = {
        key.strip(): value
        for key, value in [cooky.split('=') for cooky in cookies.split(';')]
    }
    if user_id_cookie not in cookies:
        return not_authenticated_response

    user_id = cookies[user_id_cookie]
    with SessionFactory() as session:
        tasks = session.query(models.Task).filter_by(user_id=user_id).all()
    tasks = [{
        'user_id': task.user_id,
        'response': task.response,
        'image_url': task.image_url,
        'date': task.date
    } for task in tasks]
    context = {"request": request, "tasks": tasks}
    return templates.TemplateResponse("tasks.html", context=context)
