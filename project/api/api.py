from fastapi import APIRouter
from .endpoints import account, home, tasks

router = APIRouter()
router.include_router(account.router, prefix='/account')
router.include_router(home.router, prefix='/home')
router.include_router(tasks.router, prefix='/tasks')
