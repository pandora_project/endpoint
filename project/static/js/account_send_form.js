function sendForm(pathway) {
    // Send form and set credentials cookies

    let xhr = new XMLHttpRequest();
    xhr.open("POST", pathway);
    xhr.onload = function(event) {
        if (event.target.status === 201) {
            let response_obj = JSON.parse(event.target.response);
            for (let cookie in response_obj.cookie) {
                document.cookie = response_obj.cookie[cookie];
            }
            window.location.replace(window.location.origin + "/home/");
        } else {
            console.log(event.target.status);
            console.log(event.target.response);
        }
    };

    let formData = new FormData(document.getElementsByTagName("form")[0]);
    xhr.send(formData);
}
