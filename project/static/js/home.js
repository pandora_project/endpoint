function sendTaskForm(pathway) {
    // Send form and set credentials cookies

    let xhr = new XMLHttpRequest();
    xhr.open("POST", "/home/");
    xhr.setRequestHeader("Authorization", getCookie("Authorization"));
    xhr.onload = function (event) {
        if (event.target.status === 201) {
            document.getElementById("status_bar").innerText = "success";
        } else {
            document.getElementById("status_bar").innerText =
                event.target.response;
            console.log(event.target.status);
            console.log(event.target.response);
        }
    };

    let formData = new FormData(document.getElementsByTagName("form")[0]);
    xhr.send(formData);
}

document.getElementById("send_task_button").disabled = true;
// Clear url field
document
    .getElementById("image_input")
    .addEventListener("click", clickInputUrl, event);
function clickInputUrl(event) {
    document.getElementById("status_bar").innerText = "";
    event.target.value = "";
}
document
    .getElementById("image_input")
    .addEventListener("change", setImage, event);
function setImage() {
    document.getElementById("send_task_button").disabled = false;
    document.getElementById("image_field").src = event.target.value;
}
document.getElementById("image_field").onerror = function (event) {
    document.getElementById("status_bar").innerText =
        "Incorrect image url:( Try another one.";
    document.getElementById("send_task_button").disabled = true;
};
