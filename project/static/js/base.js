const getCookie = function(name) {
    let matches = document.cookie.match(
        new RegExp(
            "(?:^|; )" +
                name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1") +
                "=([^;]*)"
        )
    );
    return matches ? decodeURIComponent(matches[1]) : undefined;
};

if (getCookie("Authorization") == undefined) {
    document.getElementById("sign_out").style.display = "none";
} else {
    document.getElementById("sign_in").style.display = "none";
    document.getElementById("sign_up").style.display = "none";
}

document.getElementById("sign_out").onclick = () => {
    document.cookie =
        "Authorization" + "=; Path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
    document.cookie =
        "UserId" + "=; Path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
    document.cookie =
        "UserName" + "=; Path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
};
