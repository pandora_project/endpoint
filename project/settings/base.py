import os

origins = [
    "http://" + os.environ["REST_HOST"] + ":" + os.environ["REST_PORT"],
    "https://" + os.environ["REST_HOST"] + ":" + os.environ["REST_PORT"],
]

PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
VOLUMES_PATH = os.path.join(os.path.dirname(PROJECT_ROOT), "volumes")
LIBRARY_PATH = os.path.join(VOLUMES_PATH, "library")

TEMPLATES_DIR = "project/templates"
STATIC_DIR = "project/static"
