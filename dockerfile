FROM davydofdmitry/python:3.75-source

RUN python3.7 -m pip install pipenv
COPY Pipfile Pipfile.lock /HOME/pandora/
WORKDIR /HOME/pandora
RUN python3.7 -m pipenv install --deploy --ignore-pipfile

COPY . /HOME/pandora
COPY playbooks/hosts /etc/ansible/hosts
